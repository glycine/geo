package exceptions

import java.lang.RuntimeException

class GeoException(msg: String): RuntimeException(msg) {
}